{block name='productdetails-details'}
{has_boxes position='left' assign='hasLeftBox'}
{if isset($bWarenkorbHinzugefuegt) && $bWarenkorbHinzugefuegt}
    {include file='productdetails/pushed_success.tpl'}
{else}
    {$alertList->displayAlertByKey('productNote')}
{/if}

<div class="h1 visible-xs text-center">{$Artikel->cName}</div>

{opcMountPoint id='opc_before_buy_form'}

<form id="buy_form" method="post" action="{$Artikel->cURLFull}" class="evo-validate">
    {$jtl_token}
    <div class="row product-primary" id="product-offer">
        <div class="product-gallery {if $hasLeftBox}col-sm-5{else}col-sm-6{/if}">
            {opcMountPoint id='opc_before_gallery'}
            {include file='productdetails/image.tpl'}
        </div>
        <div class="product-info {if $hasLeftBox}col-sm-7{else}col-sm-6{/if}">
            {block name='productdetails-info'}
            <div class="product-info-inner">
                {block name='productdetails-info-manufacturer-wrapper'}
                {if $Einstellungen.artikeldetails.artikeldetails_hersteller_anzeigen !== 'N' && isset($Artikel->cHersteller)}
                    {block name='product-info-manufacturer'}
                    <div class="manufacturer-row text-right small" itemprop="brand" itemscope itemtype="http://schema.org/Organization">
                        <a href="{$Artikel->cHerstellerSeo}"{if $Einstellungen.artikeldetails.artikeldetails_hersteller_anzeigen !== 'B'} data-toggle="tooltip" data-placement="left" title="{$Artikel->cHersteller}"{/if} itemprop="url">
                            {if $Einstellungen.artikeldetails.artikeldetails_hersteller_anzeigen !== 'Y' && (!empty($Artikel->cBildpfad_thersteller) || $Einstellungen.artikeldetails.artikeldetails_hersteller_anzeigen === 'B') && isset($Artikel->cHerstellerBildKlein)}
                                <img src="{$Artikel->cHerstellerBildURLKlein}" alt="{$Artikel->cHersteller}" class="img-sm">
                                <meta itemprop="image" content="{$Artikel->cHerstellerBildURLKlein}">
                            {/if}
                            {if $Einstellungen.artikeldetails.artikeldetails_hersteller_anzeigen !== 'B'}
                                <span itemprop="name">{$Artikel->cHersteller}</span>
                            {/if}
                        </a>
                    </div>
                    {/block}
                {/if}
                {/block}

                <div class="product-headline hidden-xs">
                    {block name='productdetails-info-product-title'}
                        {opcMountPoint id='opc_before_headline'}
                        <h1 class="fn product-title" itemprop="name">{$Artikel->cName}</h1>
                    {/block}
                </div>

                {block name='productdetails-info-essential-wrapper'}
                {if ($Artikel->Bewertungen->oBewertungGesamt->nAnzahl > 0) || isset($Artikel->cArtNr)}
                    <div class="info-essential row">
                        {block name='productdetails-info-essential'}
                        {if isset($Artikel->cArtNr) || isset($Artikel->dMHD)}
                            <div class="col-xs-8">
                                <p class="text-muted product-sku">{lang key='sortProductno'}: <span itemprop="sku">{$Artikel->cArtNr}</span></p>
                                {if isset($Artikel->dMHD) && isset($Artikel->dMHD_de)}
                                    <p title="{lang key='productMHDTool'}" class="best-before text-muted">{lang key='productMHD'}: <span itemprop="best-before">{$Artikel->dMHD_de}</span></p>
                                {/if}
                            </div>
                        {/if}
                        {if !empty($Artikel->cBarcode)
                            && ($Einstellungen.artikeldetails.gtin_display === 'details'
                                || $Einstellungen.artikeldetails.gtin_display === 'always')}
                            <div class="col-xs-8">
                                <p class="text-muted">{lang key='ean'}: <span itemprop="{if $Artikel->cBarcode|count_characters === 8}gtin8{else}gtin13{/if}">{$Artikel->cBarcode}</span></p>
                            </div>
                        {/if}
                        {if !empty($Artikel->cISBN)
                            && ($Einstellungen.artikeldetails.isbn_display === 'D'
                                || $Einstellungen.artikeldetails.isbn_display === 'DL')}
                            <div class="col-xs-8">
                                <p class="text-muted">{lang key='isbn'}: <span itemprop="gtin13">{$Artikel->cISBN}</span></p>
                            </div>
                        {/if}
                        {if !empty($Artikel->cUNNummer) && !empty($Artikel->cGefahrnr)
                            && ($Einstellungen.artikeldetails.adr_hazard_display === 'D'
                                || $Einstellungen.artikeldetails.adr_hazard_display === 'DL')}
                            <div class="col-xs-8">
                                <div class="title text-muted">{lang key='adrHazardSign'}:
                                    <table class="adr-table">
                                        <tr>
                                            <td>{$Artikel->cGefahrnr}</td>
                                        </tr>
                                        <tr>
                                            <td>{$Artikel->cUNNummer}</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        {/if}
                        {if ($Einstellungen.bewertung.bewertung_anzeigen === 'Y' && $Artikel->Bewertungen->oBewertungGesamt->nAnzahl > 0)}
                            {block name='productdetails-info-rating-wrapper'}
                            <div class="rating-wrapper col-xs-4 text-right" itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
                            <span itemprop="ratingValue"
                                  class="hidden">{$Artikel->Bewertungen->oBewertungGesamt->fDurchschnitt}</span>
                            <span itemprop="bestRating" class="hidden">5</span>
                            <span itemprop="worstRating" class="hidden">1</span>
                            <span itemprop="reviewCount" class="hidden">{$Artikel->Bewertungen->oBewertungGesamt->nAnzahl}</span>
                            <a href="{$Artikel->cURLFull}#tab-votes" id="jump-to-votes-tab" class="hidden-print">
                                {include file='productdetails/rating.tpl' stars=$Artikel->Bewertungen->oBewertungGesamt->fDurchschnitt total=$Artikel->Bewertungen->oBewertungGesamt->nAnzahl}
                            </a>
                            </div>{* /rating-wrapper*}
                            {/block}
                        {/if}
                        {/block}
                    </div>
                    <div class="clearfix top10"></div>
                {/if}
                {/block}

                {block name='productdetails-info-description-wrapper'}
                    {if $Einstellungen.artikeldetails.artikeldetails_kurzbeschreibung_anzeigen === 'Y' && $Artikel->cKurzBeschreibung}
                        {block name='productdetails-info-description'}
                            {opcMountPoint id='opc_before_short_desc'}
                            <div class="shortdesc" itemprop="description">
                                {$Artikel->cKurzBeschreibung}
                            </div>
                        {/block}
                        <div class="clearfix top10"></div>
                    {/if}
                    {opcMountPoint id='opc_after_short_desc'}
                {/block}

                {block name='productdetails-info-category-wrapper'}
                {assign var=i_kat value=($Brotnavi|@count)-2}
                {if $Einstellungen.artikeldetails.artikeldetails_kategorie_anzeigen === 'Y' && isset($Brotnavi[$i_kat])}
                    {block name='productdetails-info-category'}
                    <p class="product-category word-break">
                        <span class="text-muted">{lang key='category'}: </span>
                        <a href="{$Brotnavi[$i_kat]->getURLFull()}" itemprop="category">{$Brotnavi[$i_kat]->getName()}</a>
                    </p>
                    {/block}
                {/if}
                {/block}

                <div class="product-offer"{if !($Artikel->Preise->fVKNetto == 0 && $Einstellungen.global.global_preis0 === 'N')} itemprop="offers" itemscope itemtype="http://schema.org/Offer"{/if}>
                    {block name='productdetails-info-hidden'}
                    {if !($Artikel->Preise->fVKNetto == 0 && $Einstellungen.global.global_preis0 === 'N')}
                        <meta itemprop="url" content="{$Artikel->cURLFull}">
                        <link itemprop="businessFunction" href="http://purl.org/goodrelations/v1#Sell" />
                    {/if}

                    <input type="submit" name="inWarenkorb" value="1" class="hidden" />
                    {if $Artikel->kArtikelVariKombi > 0}
                        <input type="hidden" name="aK" value="{$Artikel->kArtikelVariKombi}" />
                    {/if}
                    {if isset($Artikel->kVariKindArtikel)}
                        <input type="hidden" name="VariKindArtikel" value="{$Artikel->kVariKindArtikel}" />
                    {/if}
                    {if isset($smarty.get.ek)}
                        <input type="hidden" name="ek" value="{$smarty.get.ek|intval}" />
                    {/if}
                    <input type="hidden" id="AktuellerkArtikel" class="current_article" name="a" value="{$Artikel->kArtikel}" />
                    <input type="hidden" name="wke" value="1" />
                    <input type="hidden" name="show" value="1" />
                    <input type="hidden" name="kKundengruppe" value="{$smarty.session.Kundengruppe->getID()}" />
                    <input type="hidden" name="kSprache" value="{$smarty.session.kSprache}" />
                    {/block}
                    {block name='productdetails-info-variation'}
                    <!-- VARIATIONEN -->
                    {include file='productdetails/variation.tpl' simple=$Artikel->isSimpleVariation showMatrix=$showMatrix}
                    {/block}
                    <hr>
                    <div class="row">
                        {block name='productdetails-info-price'}
                        <div class="col-xs-7">
                            {include file='productdetails/price.tpl' Artikel=$Artikel tplscope='detail'}
                        </div>
                        {/block}
                        {block name='productdetails-info-stock'}
                        <div class="col-xs-5 text-right">
                            {include file='productdetails/stock.tpl'}
                        </div>
                        {/block}
                    </div>
                    {*WARENKORB anzeigen wenn keine variationen mehr auf lager sind?!*}
                    {include file='productdetails/basket.tpl'}
                    <hr>
                </div>

                {if !($Artikel->nIstVater && $Artikel->kVaterArtikel == 0)}
                    {include file='productdetails/actions.tpl'}
                {/if}
            </div>{* /product-info-inner *}
            {/block}{* productdetails-info *}
            {opcMountPoint id='opc_after_product_info'}
        </div>{* /col *}
        {if $Artikel->bHasKonfig}
            {block name='productdetails-config'}
            <div id="product-configurator" class="col-sm-12">
                <div class="product-config top10">
                    {*KONFIGURATOR*}
                    {include file='productdetails/config.tpl'}
                </div>
            </div>
            {/block}
        {/if}
    </div>{* /row *}
    {block name='details-matrix'}
    {include file='productdetails/matrix.tpl'}
    {/block}
</form>

{if !isset($smarty.get.quickView) || $smarty.get.quickView != 1}
    <div class="clearfix"></div>

    {block name='details-tabs'}
    {include file='productdetails/tabs.tpl'}
    {/block}

    <div class="clearfix"></div>

    {*SLIDERS*}
    {if isset($Einstellungen.artikeldetails.artikeldetails_stueckliste_anzeigen) && $Einstellungen.artikeldetails.artikeldetails_stueckliste_anzeigen === 'Y' && isset($Artikel->oStueckliste_arr) && $Artikel->oStueckliste_arr|@count > 0
        || isset($Einstellungen.artikeldetails.artikeldetails_produktbundle_nutzen) && $Einstellungen.artikeldetails.artikeldetails_produktbundle_nutzen === 'Y' && isset($Artikel->oProduktBundle_arr) && $Artikel->oProduktBundle_arr|@count > 0
        || isset($Xselling->Standard->XSellGruppen) && count($Xselling->Standard->XSellGruppen) > 0
        || isset($Xselling->Kauf->Artikel) && count($Xselling->Kauf->Artikel) > 0
        || isset($oAehnlicheArtikel_arr) && count($oAehnlicheArtikel_arr) > 0}
        <hr>
        {if isset($Einstellungen.artikeldetails.artikeldetails_stueckliste_anzeigen) && $Einstellungen.artikeldetails.artikeldetails_stueckliste_anzeigen === 'Y' && isset($Artikel->oStueckliste_arr) && $Artikel->oStueckliste_arr|@count > 0}
            <div class="partslist">
                {lang key='listOfItems' section='global' assign='slidertitle'}
                {include file='snippets/product_slider.tpl' id='slider-partslist' productlist=$Artikel->oStueckliste_arr title=$slidertitle showPartsList=true}
            </div>
        {/if}

        {if isset($Einstellungen.artikeldetails.artikeldetails_produktbundle_nutzen) && $Einstellungen.artikeldetails.artikeldetails_produktbundle_nutzen === 'Y' && isset($Artikel->oProduktBundle_arr) && $Artikel->oProduktBundle_arr|@count > 0}
            <div class="bundle">
                {include file='productdetails/bundle.tpl' ProductKey=$Artikel->kArtikel Products=$Artikel->oProduktBundle_arr ProduktBundle=$Artikel->oProduktBundlePrice ProductMain=$Artikel->oProduktBundleMain}
            </div>
        {/if}

        {if isset($Xselling->Standard) || isset($Xselling->Kauf) || isset($oAehnlicheArtikel_arr)}
            <div class="recommendations hidden-print">
                {block name='productdetails-recommendations'}
                {if isset($Xselling->Standard->XSellGruppen) && count($Xselling->Standard->XSellGruppen) > 0}
                    {foreach $Xselling->Standard->XSellGruppen as $Gruppe}
                        {include file='snippets/product_slider.tpl' class='x-supplies' id='slider-xsell-group-'|cat:$Gruppe@iteration productlist=$Gruppe->Artikel title=$Gruppe->Name}
                    {/foreach}
                {/if}

                {if isset($Xselling->Kauf->Artikel) && count($Xselling->Kauf->Artikel) > 0}
                    {lang key='customerWhoBoughtXBoughtAlsoY' section='productDetails' assign='slidertitle'}
                    {include file='snippets/product_slider.tpl' class='x-sell' id='slider-xsell' productlist=$Xselling->Kauf->Artikel title=$slidertitle}
                {/if}

                {if isset($oAehnlicheArtikel_arr) && count($oAehnlicheArtikel_arr) > 0}
                    {lang key='RelatedProducts' section='productDetails' assign='slidertitle'}
                    {include file='snippets/product_slider.tpl' class='x-related' id='slider-related' productlist=$oAehnlicheArtikel_arr title=$slidertitle}
                {/if}
                {/block}
            </div>
        {/if}
    {/if}
    <div id="article_popups">
        {include file='productdetails/popups.tpl'}
    </div>
{/if}
{/block}
