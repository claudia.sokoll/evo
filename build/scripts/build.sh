#!/usr/bin/env bash

build_create()
{
    # $1 repository dir
    export REPOSITORY_DIR=$1;
    # $2 target build version
    export APPLICATION_VERSION=$2;

    local PHAR_READONLY=$(php -r "print(ini_get('phar.readonly'));");
    local SCRIPT_DIR="${REPOSITORY_DIR}/build/scripts";

    source ${SCRIPT_DIR}/create_version_string.sh;

    if [[ ${PHAR_READONLY} == 1 ]]; then
        echo "In order to create a phar-archive set disable 'phar.readonly'";
        exit 1;
    fi

    echo "Set composer version";
    create_version_string ${REPOSITORY_DIR} ${APPLICATION_VERSION};
    if [[ ! -z "${NEW_VERSION}" ]]; then
        export APPLICATION_VERSION_STR=${NEW_VERSION};
    else
        export APPLICATION_VERSION_STR=${APPLICATION_VERSION};
    fi
    composer config version ${APPLICATION_VERSION_STR} -d ${REPOSITORY_DIR};

    echo "Executing composer";
    composer install --no-dev -q -d ${REPOSITORY_DIR};
}


#(build_create $*)
echo "build process skipped. (nothing to do)"
