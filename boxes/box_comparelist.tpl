{block name='boxes-box-comparelist'}
{if $Einstellungen.vergleichsliste.vergleichsliste_anzeigen === 'Y'}
{assign var=maxItems value=$oBox->getItemCount()}
{assign var=itemCount value=count($oBox->getProducts())}
{if $itemCount > 0}
    <section class="panel panel-default box box-compare" id="sidebox{$oBox->getID()}">
        <div class="panel-heading">
            <div class="panel-title"><i class="fa fa-tasks"></i> {lang key='compare'}</div>
        </div>
        <div class="box-body panel-body">
            <ul class="list-unstyled">
                {foreach $oBox->getProducts() as $oArtikel}
                    {if $oArtikel@iteration > $maxItems}
                        {break}
                    {/if}
                    <li data-id="{$oArtikel->kArtikel}">
                        <a href="{$oArtikel->cURLDEL}" class="remove pull-right" data-name="Vergleichsliste.remove"
                           data-toggle="product-actions" data-value='{ldelim}"a":{$oArtikel->kArtikel}{rdelim}'><span
                                    class="fa fa-trash-o"></span></a>
                        <a href="{$oArtikel->cURLFull}">
                            <img src="{$oArtikel->Bilder[0]->cURLMini}"
                                 alt="{$oArtikel->cName|strip_tags|truncate:60|escape:'html'}" class="img-xs"/>
                            {$oArtikel->cName|truncate:25:'...'}
                        </a>
                    </li>
                {/foreach}
            </ul>
            {if $itemCount > 1}
                <hr>
                <a class="btn btn-default btn-sm btn-block{if $Einstellungen.vergleichsliste.vergleichsliste_target === 'popup'} popup{/if}"
                    href="{get_static_route id='vergleichsliste.php'}"{if $Einstellungen.vergleichsliste.vergleichsliste_target === 'blank'}
                    target="_blank"{/if}>{lang key='gotToCompare'}</a>
            {/if}
        </div>
    </section>
{else}
    <section class="hidden box-compare" id="sidebox{$oBox->getID()}"></section>
{/if}
{/if}
{/block}
