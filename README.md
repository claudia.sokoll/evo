# JTL-Shop Evo Template für JTL-Shop 5 #

<br>

Das Standardtemplate des JTL-Shop 4 - hier jedoch mit Kompatibilitätsänderungen für die Nutzung in JTL-Shop 5! 

Hinweise zum Update auf jQuery 3.x: https://jtl-devguide.readthedocs.io/projects/jtl-shop/de/latest/shop_templates/jquery_update.html 